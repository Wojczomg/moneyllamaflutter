const admin = require("firebase-admin");
const functions = require("firebase-functions");
let xmlParser = require("xml2json");
const docusign = require("docusign-esign");
const BASEPATH = "https://demo.docusign.net/restapi";
const ACCESSTOKEN =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjY4MTg1ZmYxLTRlNTEtNGNlOS1hZjFjLTY4OTgxMjIwMzMxNyJ9.eyJUb2tlblR5cGUiOjUsIklzc3VlSW5zdGFudCI6MTU5ODAwNTAyNCwiZXhwIjoxNTk4MDMzODI0LCJVc2VySWQiOiI3N2E5NTg2Yi03YjQ1LTQwYjYtODFiMy03MjA3MmU0ODFkYzUiLCJzaXRlaWQiOjEsInNjcCI6WyJzaWduYXR1cmUiLCJjbGljay5tYW5hZ2UiLCJvcmdhbml6YXRpb25fcmVhZCIsInJvb21fZm9ybXMiLCJncm91cF9yZWFkIiwicGVybWlzc2lvbl9yZWFkIiwidXNlcl9yZWFkIiwidXNlcl93cml0ZSIsImFjY291bnRfcmVhZCIsImRvbWFpbl9yZWFkIiwiaWRlbnRpdHlfcHJvdmlkZXJfcmVhZCIsImR0ci5yb29tcy5yZWFkIiwiZHRyLnJvb21zLndyaXRlIiwiZHRyLmRvY3VtZW50cy5yZWFkIiwiZHRyLmRvY3VtZW50cy53cml0ZSIsImR0ci5wcm9maWxlLnJlYWQiLCJkdHIucHJvZmlsZS53cml0ZSIsImR0ci5jb21wYW55LnJlYWQiLCJkdHIuY29tcGFueS53cml0ZSJdLCJhdWQiOiJmMGYyN2YwZS04NTdkLTRhNzEtYTRkYS0zMmNlY2FlM2E5NzgiLCJhenAiOiJmMGYyN2YwZS04NTdkLTRhNzEtYTRkYS0zMmNlY2FlM2E5NzgiLCJpc3MiOiJodHRwczovL2FjY291bnQtZC5kb2N1c2lnbi5jb20vIiwic3ViIjoiNzdhOTU4NmItN2I0NS00MGI2LTgxYjMtNzIwNzJlNDgxZGM1IiwiYXV0aF90aW1lIjoxNTk4MDA0MDgwLCJwd2lkIjoiODBjOWMwZDAtYmQzMi00MjE1LWJkZjQtODcyMDY2ZjU3MzZhIn0.kwEUO-unhhcUyMxifRRL9UcvmmBvXCBFEajcwoq6qPUR6oPgYQKt9ax2mzPaVRrIw5PKVAUkOpzFHF2Pm8LkjXfVPmilb6fU4eO69VO36qvRXqrTIlICgSZDJ_m7s5tV5y5Jy1UFxj6pU6KT3V6LyvbxAtOst2l3yKxKTI8UuhZgJQeQEn6UCXC-erJt1CX4nGKzII4E7ptPWyeApSwV5KwLO-PIveOYDKcUmy0mVan7zw1GbFgHbVZOvbBElHvm4ueB2ERvQbVcjW4Fj62jlgA6YwITgsIK0BcC4uTA-jRwOGw0uqYrXA_lXXX6yNRwsh2pUBOeFlTMQL6fgwwPZg";
const ACCOUNTID = "e59ea339-6ea2-4aa5-ad4b-f3aa30d04d96";
admin.initializeApp(functions.config().firebase);

let db = admin.firestore();

exports.newUser = functions.firestore
  .document("users/{userId}")
  .onCreate(async (snap, context) => {
    const mainEmail = snap.data().mainEmail;
    const secretCode = snap.data().adresses[mainEmail].authCode;
    const name = snap.data().name;
    let ref = db.collection("mail");
    let res = await ref.add({
      to: mainEmail,
      message: {
        subject: "Witamy w Money Llama",
        html: `
        <p>Witaj ${name}</p>
        <p></p>
        <p>
        Cieszymy się, że dołączyłeś/aś do Money Llama. Od dziś zarządzanie Twoimi subskrypcjami będzie proste i wygodne! Pomożemy Ci kontrolować Twoje cykliczne wydatki oraz doradzimy, gdzie możesz zaoszczędzić.
        W celu zakończenia rejestracji i dodania adresu email  do swojego konta Money Llama skopiuj poniższy kod i wklej w aplikacji mobilnej. 
        </p>
        <p></p>
        <p>${secretCode}</p>
        <p>
        W razie pytań znajdziesz nas na hello@moneyllama.com
        </p>
        `,
      },
    });
    functions.logger.log(res);
    return;
  });

exports.webhook = functions.https.onRequest((request, repsonse) => {
  functions.logger.log(xmlParser.toJson(request.body.toString()));
  let json = xmlParser.toJson(request.body.toString());
  const email = JSON.parse(json).DocuSignEnvelopeInformation.EnvelopeStatus
    .RecipientStatuses.RecipientStatus.Email;
  const uid = JSON.parse(json).DocuSignEnvelopeInformation.EnvelopeStatus
    .CustomFields.CustomField.Value;
  const documentId = JSON.parse(json).DocuSignEnvelopeInformation.EnvelopeStatus
    .RecipientStatuses.RecipientStatus.TabStatuses.TabStatus.DocumentID;
  const envelopeId = JSON.parse(json).DocuSignEnvelopeInformation.EnvelopeStatus
    .EnvelopeID;
  functions.logger.log(uid);
  functions.logger.log(email);
  functions.logger.log(documentId);
  functions.logger.log(envelopeId);
  db.collection("users")
    .doc(uid)
    .set(
      {
        adresses: {
          [email]: {
            attorney: true,
            documentId: documentId,
            envelopeId: envelopeId,
          },
        },
      },
      { merge: true }
    )
    .then((res) => repsonse.send("mam"));
});

exports.subCancelRequest = functions.https.onRequest(
  async (request, response) => {
    const args = {
      basePath: BASEPATH,
      accessToken: ACCESSTOKEN,
      accountId: ACCOUNTID,
      envelopeDocuments: {
        envelopeId: request.query.envelopeId,
      },
      documentId: request.query.documentId,
      sendTo: request.query.sendTo,
    };
    let dsApiClient = new docusign.ApiClient();
    dsApiClient.setBasePath(args.basePath);
    dsApiClient.addDefaultHeader("Authorization", "Bearer " + args.accessToken);
    let envelopesApi = new docusign.EnvelopesApi(dsApiClient);
    try {
      let results = await envelopesApi.getDocument(
        args.accountId,
        args.envelopeDocuments.envelopeId,
        args.documentId,
        null
      );
      let ref = db.collection("mail");
      let res = await ref.add({
        // to: "pexon12345@gmail.com",
        to: args.sendTo,
        message: {
          subject: "Subsciption cancel request",
          html: "Subsciption cancel request",
          attachments: [
            {
              filename: "upowaznienie.pdf",
              content: Buffer.from(results, "binary"),
            },
          ],
        },
      });
      response.status(200).end();
    } catch (err) {
      functions.logger.log(err);
      response.status(500).end();
    }
  }
);

exports.sendAttorney = functions.https.onRequest(async (req, res) => {
  const qp = req.query;

  const accessToken = ACCESSTOKEN;

  // Obtain your accountId from demo.docusign.com -- the account id is shown in the drop down on the
  // upper right corner of the screen by your picture or the default picture.
  const accountId = ACCOUNTID;

  // Recipient Information:
  const signerName = `${qp.name} ${qp.surname}`;

  const signerEmail = qp.email;

  // The document you wish to send. Path is relative to the root directory of this repo.
  // const fileName = "demo_documents/World_Wide_Corp_lorem.pdf";

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *  The envelope is sent to the provided email address.
   *  One signHere tab is added.
   *  The document path supplied is relative to the working directory
   */
  const apiClient = new docusign.ApiClient();
  apiClient.setBasePath(BASEPATH);
  apiClient.addDefaultHeader("Authorization", "Bearer " + accessToken);
  // Set the DocuSign SDK components to use the apiClient object
  docusign.Configuration.default.setDefaultApiClient(apiClient);

  // Create the envelope request
  // Start with the request object
  const envDef = new docusign.EnvelopeDefinition();
  //Set the Email Subject line and email message
  envDef.emailSubject = "Prośba o upowanienie dla MoneyLlama";
  envDef.emailBlurb = "Prośba o upowanienie dla MoneyLlama";

  // Read the file from the document and convert it to a Base64String
  // const pdfBytes = fs.readFileSync(path.resolve(__dirname, fileName))
  // let pdfBase64 = pdfBytes.toString("base64");

  let html = `<body style="margin:30px"><h1 style="text-align:center;padding: 70px">Power of attorney</h1>
  <p><b>I, the undersigned acting on behalf of myself ${qp.name} ${
    qp.surname
  }, with following contact data:</b></p>
  <ul><li>E-mail adress–${qp.email}</li><li>Telephone number – +48 ${
    qp.phoneNumber
  }</li></ul>
  <br>
  <p><b>I hereby grant a power of attorney to:</b></p>
  <p>Sławomir   Grzebień,   PESEL:   90060710267,   adress:Prądzyńskiego 34, 05-080 Izabelin, Poland, owner of the emailaddress: slawomir.grzebien@gmail.com</p>
  <br>

  <p><b>Authorising him to:</b></p>
  <p>(1) conclude and initiate as well as (2)terminate agreements of electronically supplied services, that may, but do not need to bereferred as subscriptions<p>
  <p><b>DATE</b></p>
  <div>${new Date().toLocaleDateString()}</div></body>
  `;
  var pdf = require("html-pdf");
  pdf.create(html, { format: "Letter" }).toBuffer(async function (err, buffer) {
    functions.logger.log(Buffer.isBuffer(buffer));
    let pdfBase64 = buffer.toString("base64");

    // Create the document request object
    const doc = docusign.Document.constructFromObject({
      documentBase64: pdfBase64,
      fileExtension: "pdf", // You can send other types of documents too.
      name: "Sample document",
      documentId: "1",
    });

    // Create a documents object array for the envelope definition and add the doc object
    envDef.documents = [doc];

    // Create the signer object with the previously provided name / email address
    const signer = docusign.Signer.constructFromObject({
      name: signerName,
      email: signerEmail,
      routingOrder: "1",
      recipientId: "1",
    });

    // Create the signHere tab to be placed on the envelope
    const signHere = docusign.SignHere.constructFromObject({
      documentId: "1",
      pageNumber: "1",
      recipientId: "1",
      tabLabel: "SignHereTab",
      xPosition: "450",
      yPosition: "680",
    });

    // Create the overall tabs object for the signer and add the signHere tabs array
    // Note that tabs are relative to receipients/signers.
    signer.tabs = docusign.Tabs.constructFromObject({
      signHereTabs: [signHere],
    });

    // Add the recipients object to the envelope definition.
    // It includes an array of the signer objects.
    envDef.recipients = docusign.Recipients.constructFromObject({
      signers: [signer],
    });
    // Set the Envelope status. For drafts, use 'created' To send the envelope right away, use 'sent'
    envDef.status = "sent";
    envDef.eventNotification = {
      url: "https://us-central1-moneyllama-2.cloudfunctions.net/webhook ",

      envelopeEvents: [
        {
          envelopeEventStatusCode: "completed",
        },
      ],
    };
    envDef.customFields = {
      textCustomFields: [
        {
          name: "uid",
          value: qp.uid,
        },
      ],
    };

    // Send the envelope
    let envelopesApi = new docusign.EnvelopesApi(),
      results;

    try {
      results = await envelopesApi.createEnvelope(accountId, {
        envelopeDefinition: envDef,
      });
    } catch (e) {
      let body = e.response && e.response.body;
      if (body) {
        // DocuSign API exception
        res.send(`<html lang="en"><body>
                  <h3>API problem</h3><p>Status code ${e.response.status}</p>
                  <p>Error message:</p><p><pre><code>${JSON.stringify(
                    body,
                    null,
                    4
                  )}</code></pre></p>`);
      } else {
        // Not a DocuSign exception
        throw e;
      }
    }
    // Envelope has been created:
    if (results) {
      res.send(`<html lang="en"><body>
                <h3>Envelope Created!</h3>
                <p>Signer: ${signerName} &lt;${signerEmail}&gt;</p>
                <p>Results</p><p><pre><code>${JSON.stringify(
                  results,
                  null,
                  4
                )}</code></pre></p>`);
    }
  });
});
