List<Map> logos = [
  {
    'name': 'Netflix',
    'imagePath': 'assets/logos/netflix-logo-circle-png-5.png',
    'privacyDepEmail':'privacy@netflix.com',
  },
  {
    'name': 'HBO GO',
    'imagePath': 'assets/logos/hbogologo.png',
    'privacyDepEmail':'dpo@hbo.eu',
  },
  {
    'name': 'T-Mobile',
    'imagePath': 'assets/logos/tmobilelogo.png',
    'privacyDepEmail':'IOD@t-mobile.pl',
  },
  {
    'name': 'PLAY',
    'imagePath': 'assets/logos/logo-PLAY.png',
    'privacyDepEmail':'iod@pomocplay.pl',
  },
  {
    'name': 'Orange',
    'imagePath': 'assets/logos/766px-Orange_logo.svg.png',
    'privacyDepEmail':'inspektorochronydanych@orange.com',
  },
  {
    'name': 'Plus',
    'imagePath': 'assets/logos/Plus-Logo-1.png',
    'privacyDepEmail':'iod@plus.pl',
  },
  {
    'name': 'Spotify',
    'imagePath': 'assets/logos/spotifylogo.png',
    'privacyDepEmail':'privacy@spotify.com',
  },
  {
    'name': 'YouTube',
    'imagePath': 'assets/logos/youtubelogo.png',
    'privacyDepEmail':'-',
  },
  {
    'name': 'PlayStation Network',
    'imagePath': 'assets/logos/1280px-PlayStation_logo.svg.png',
    'privacyDepEmail':'dpo@scee.net',
  },
  {
    'name': 'Xbox Game Pass',
    'imagePath': 'assets/logos/kbox-game-pass-logo.png',
    'privacyDepEmail':'-',
  },
  {
    'name': 'Tidal',
    'imagePath': 'assets/logos/tidallogo.png',
    'privacyDepEmail':'dpo@tidal.com',
  },
  {
    'name': 'Skillshare',
    'imagePath': 'assets/logos/skillsharelogo.png',
    'privacyDepEmail':'help@skillshare.com',
  },
  {
    'name': 'World of Warcraft',
    'imagePath': 'assets/logos/wowlogo.png',
    'privacyDepEmail':'DPO@Blizzard.com',
  },
  {
    'name': 'TeamSpeak',
    'imagePath': 'assets/logos/teamspeaklogo.png',
    'privacyDepEmail':'privacy@teamspeak.com',
  },
  {
    'name': 'UPC',
    'imagePath': 'assets/logos/upclogo.png',
    'privacyDepEmail':'iod@upc.pl',
  },
  {
    'name': 'BeActive',
    'imagePath': 'assets/logos/beactivelogo.jpg',
    'privacyDepEmail':'info@ebeactive.pl',
  },
  {
    'name': 'Amazon Prime Video',
    'imagePath': 'assets/logos/amazonprimevideologo.png',
    'privacyDepEmail':'privacyshield@amazon.com',
  },
  {
    'name': 'Twitch Prime',
    'imagePath': 'assets/logos/twitchlogo.jpg',
    'privacyDepEmail':'privacyshield@amazon.com',
  },
  {
    'name': 'Storytel',
    'imagePath': 'assets/logos/storytellogo.jpg',
    'privacyDepEmail':'privacy@storytel.com',
  },
  {
    'name': 'Audioteka',
    'imagePath': 'assets/logos/audioekalogo.png',
    'privacyDepEmail':'iod@audioteka.com',
  },
  {
    'name': 'Pluralsight',
    'imagePath': 'assets/logos/pluralsightlogo.png',
    'privacyDepEmail':'support@pluralsight.com',
  },
  {
    'name': 'Origin Access',
    'imagePath': 'assets/logos/originlogo.jpg',
    'privacyDepEmail':'dpo@ea.com',
  },
  {
    'name': 'Allegro Smart',
    'imagePath': 'assets/logos/allegrosmartlogo.jpg',
    'privacyDepEmail':'iod@allegro.pl',
  },
  {
    'name': 'Nice to fit you',
    'imagePath': 'assets/logos/nicetofitulogo.png',
    'privacyDepEmail':'iod@ntfy.pl',
  },
  {
    'name': 'Cinema City UNLIMITED',
    'imagePath': 'assets/logos/cinemacitylogo.jpg',
    'privacyDepEmail':'IOD@cinema-city.pl',
  },
];