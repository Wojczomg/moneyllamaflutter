class SubscriptionData {
  final id;
  final title;
  final email;
  final price;
  final imageUrl;
  String date;
  bool isCancelled;
  String privacyDepEmail;
  SubscriptionData({
    this.id,
    this.email,
    this.imageUrl,
    this.price,
    this.title,
    this.date,
    this.isCancelled,
    this.privacyDepEmail,
  });
}

List dummyData = [
  SubscriptionData(
    title: 'Netflix',
    email: 'maail@gmail.com',
    price: 52.0,
    imageUrl: 'assets/logos/netflix-logo-circle-png-5.png',
  ),
  SubscriptionData(
    title: 'YouTube',
    email: 'maail@gmail.com',
    price: 30.0,
    imageUrl: 'assets/logos/youtubelogo.png',
  ),
  SubscriptionData(
    title: 'World of Warcraft',
    email: 'maail@gmail.com',
    price: 11.0,
    imageUrl: 'assets/logos/wowlogo.png',
  ),
  SubscriptionData(
    title: 'PSN',
    email: 'maail@gmail.com',
    price: 73.0,
    imageUrl: 'assets/logos/1280px-PlayStation_logo.svg.png',
  ),
];
