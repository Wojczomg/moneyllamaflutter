import 'package:flutter/material.dart';
import 'package:moneylama2/screens/AuthScreen.dart';
import 'package:moneylama2/screens/AuthWrapper.dart';
import 'package:moneylama2/screens/HomePage.dart';
import 'package:moneylama2/screens/MainScreenWrapper.dart';
import 'package:moneylama2/screens/SettingsScreen.dart';
import 'package:moneylama2/services/auth_service.dart';
import 'package:moneylama2/steps_widgets/create_account/attorney.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AuthService>.value(
      value: AuthService(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            primaryColor: Color(0xffA0447E),
            fontFamily: 'BalooPaaji2',
            textTheme: TextTheme(
              body1: TextStyle(height: 1),
              body2: TextStyle(height: 1),
              button: TextStyle(height: 1),
              caption: TextStyle(height: 1),
              display1: TextStyle(height: 1),
              display2: TextStyle(height: 1),
              display3: TextStyle(height: 1),
              display4: TextStyle(height: 1),
              headline: TextStyle(height: 1),
              overline: TextStyle(height: 1),
              subhead: TextStyle(height: 1),
              subtitle: TextStyle(height: 1),
              title: TextStyle(height: 1),
            )),
        home: Wrapper(),
       
      ),
    );
  }
}
