import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';
import 'package:moneylama2/DUMMY_DATA.dart';
import 'package:moneylama2/LOGOS_PATHS.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';
import 'package:provider/provider.dart';
import '../DUMMY_DATA.dart';

class EditSubScreen extends StatefulWidget {
  Function goToHomePage;
  SubscriptionData sub;
  EditSubScreen(
    this.goToHomePage, {
    this.sub,
  });
  @override
  _EditSubScreenState createState() => _EditSubScreenState();
}

class _EditSubScreenState extends State<EditSubScreen> {
  TextEditingController subNameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String currentImage;

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('AlertDialog Title'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Ups'),
                Text('Podczas rejestracji nowej subskrypcji wystąpił błąd.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    currentImage = widget.sub.imageUrl;
    subNameController.text = widget.sub.title;
    priceController.text = widget.sub.price.toString();
    emailController.text = widget.sub.email;
    dateController.text = widget.sub.date;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List hints = logos;
    DatabaseService db = Provider.of<DatabaseService>(context, listen: false);
    Map userData = db.userData;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CurvedShape(),
          SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height - 60,
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 18.0,
                        left: 18,
                        right: 18,
                      ),
                      child: Text(
                        'Dodaj subskrypcję',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 22),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(6),
                          ),
                        ),
                        margin:
                            EdgeInsets.symmetric(horizontal: 18, vertical: 18),
                        child: Padding(
                          padding: EdgeInsets.all(18),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'Podaj dane dotyczące subskrypcji',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 22,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                // TextFormField(
                                //   controller: subNameController,
                                //   decoration: InputDecoration(
                                //     labelText: 'Nazwa usługi',
                                //   ),
                                // ),
                                TypeAheadFormField(
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                    controller: subNameController,
                                    decoration: InputDecoration(
                                      labelText: 'Nazwa usługi',
                                    ),
                                  ),
                                  suggestionsCallback: (pattern) {
                                    if (pattern.isEmpty) {
                                      return null;
                                    }
                                    RegExp reg =
                                        RegExp(pattern, caseSensitive: false);
                                    return hints.where((elem) {
                                      return reg.hasMatch(elem['name']);
                                    });
                                  },
                                  itemBuilder: (context, suggestion) {
                                    return Container(
                                      margin: EdgeInsets.symmetric(vertical: 6),
                                      child: ListTile(
                                        leading: Image.asset(
                                            suggestion['imagePath']),
                                        title: Text(
                                          suggestion['name'],
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    subNameController.text = suggestion['name'];
                                    setState(() {
                                      currentImage = suggestion['imagePath'];
                                    });
                                  },
                                ),
                                TextFormField(
                                  controller: priceController,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    labelText: 'Opłata miesięczna',
                                  ),
                                ),
                                TextFormField(
                                  readOnly: true,
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return SimpleDialog(
                                          children: userData['emailList']
                                              .map<Widget>(
                                                (email) => ListTile(
                                                  title: Text(email),
                                                  leading: Radio(
                                                    value: email,
                                                    groupValue:
                                                        emailController.text,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        emailController.text =
                                                            value;
                                                      });
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                  ),
                                                ),
                                              )
                                              .toList(),
                                        );
                                      },
                                    );
                                  },
                                  controller: emailController,
                                  decoration: InputDecoration(
                                    labelText: 'Adres email',
                                  ),
                                ),
                                TextFormField(
                                  controller: dateController,
                                  readOnly: true,
                                  onTap: () {
                                    showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(2020),
                                      lastDate: DateTime(2021),
                                      builder:
                                          (BuildContext context, Widget child) {
                                        return Theme(
                                          data: ThemeData.light().copyWith(
                                            primaryColor:
                                                const Color(0xffA0447E),
                                            accentColor:
                                                const Color(0xffA0447E),
                                            colorScheme: ColorScheme.light(
                                                primary:
                                                    const Color(0xffA0447E)),
                                          ),
                                          child: child,
                                        );
                                      },
                                    ).then((date) => {
                                          setState(() {
                                            dateController.text =
                                                DateFormat.MMMd().format(date);
                                          })
                                        });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Data naliczenia opłaty',
                                  ),
                                ),
                                currentImage == null
                                    ? Container()
                                    : Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.all(20.0),
                                          child: Image.asset(
                                            currentImage,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(18),
                    width: double.infinity,
                    child: RaisedButton(
                      onPressed: () async {
                       
                        await db.deleteService(
                          email: widget.sub.email,
                          id: widget.sub.id,
                        );
                        var result = await db.addNewService(
                          title: subNameController.text,
                          price: double.parse(priceController.text),
                          email: emailController.text,
                          imageUrl: currentImage,
                          date: dateController.text,
                          isCancelled: widget.sub.isCancelled,
                        );
                        if (!result) {
                          _showMyDialog();
                          return;
                        }
                        widget.goToHomePage();
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      elevation: 0,
                      color: Color(0xffFED766),
                      child: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Text(
                          'Dodaj',
                          style: TextStyle(
                              color: Color(0xffA0447E),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
