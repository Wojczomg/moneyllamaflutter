import 'package:flutter/material.dart';
import 'package:moneylama2/screens/Register.dart';
import 'package:moneylama2/screens/SignInScreen.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {

  bool showRegister = false;
  bool showSignIn = false;
  bool showWelcome = false;
  void toggleView(){
    setState(() {
      showRegister = false;
      showSignIn = false;
      showWelcome = !showWelcome;
      });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Stack(
        children: <Widget>[
          CurvedShape(),
          showSignIn ? SignIn(toggleView) :
          showRegister ? RegisterScreen(toggleView) :
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Image.asset('assets/llama_logo.png'),
                ),
                SizedBox(height: 100,),
                Container(
                  margin: EdgeInsets.all(12),
                  width: double.infinity,
                  child: RaisedButton(
                    onPressed: () {
                      setState(() {
                        showSignIn = true;
                      });
                    },
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                        color: Color(0xffFED766),
                        width: 5,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    elevation: 0,
                    color: Colors.transparent,
                    child: Padding(
                      padding: const EdgeInsets.all(17.0),
                      child: Text(
                        'Logowanie',
                        style: TextStyle(
                            color: Color(0xffA0447E),
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(12),
                  width: double.infinity,
                  child: RaisedButton(
                    onPressed: () {
                      setState(() {
                        showRegister = true;
                      });
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    elevation: 0,
                    color: Color(0xffFED766),
                    child: Padding(
                      padding: const EdgeInsets.all(17.0),
                      child: Text(
                        'Rejestracja',
                        style: TextStyle(
                            color: Color(0xffA0447E),
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 50,)
              ],
            ),
          )
        ],
      ),
    );
  }
}
