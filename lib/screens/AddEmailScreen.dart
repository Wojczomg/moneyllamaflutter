import 'dart:io';
import 'dart:math';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:moneylama2/screens/MainScreenWrapper.dart';
import 'package:moneylama2/services/auth_service.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:moneylama2/steps_widgets/add_adress/addAdress.dart';
import 'package:moneylama2/steps_widgets/add_adress/addAdressCode.dart';
import 'package:moneylama2/steps_widgets/create_account/attorney.dart';
import 'package:moneylama2/steps_widgets/create_account/auth-code.dart';
import 'package:moneylama2/steps_widgets/create_account/basics.dart';
import 'package:moneylama2/steps_widgets/create_account/passwords.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';
import 'package:moneylama2/widgets/FillCircle.dart';
import 'package:moneylama2/widgets/FillStripe.dart';
import 'package:provider/provider.dart';

class AddEmailScreen extends StatefulWidget {
  final Function goBack;

  AddEmailScreen(
    this.goBack,
  );
  @override
  _AddEmailScreenState createState() => _AddEmailScreenState();
}

class _AddEmailScreenState extends State<AddEmailScreen> {
  GlobalKey<FillCircleState> firstCircle = GlobalKey();
  GlobalKey<FillStripeState> firstStripe = GlobalKey();
  GlobalKey<FillCircleState> secondCircle = GlobalKey();
  GlobalKey<FillStripeState> secondStripe = GlobalKey();
  GlobalKey<FillCircleState> thirdCircle = GlobalKey();
  GlobalKey<FillStripeState> thirdStripe = GlobalKey();
  GlobalKey<FillCircleState> fourthCircle = GlobalKey();

  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController password1Controller = TextEditingController();
  TextEditingController password2Controller = TextEditingController();
  TextEditingController secretCodeController = TextEditingController();

  final _formKeyBasics = GlobalKey<FormState>();
  final _formKeyPasswords = GlobalKey<FormState>();
  final _formKeyAuthCode = GlobalKey<FormState>();

  var secretCode;

  List<Map> steps = [];

  int currentStep = 0;

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('AlertDialog Title'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Ups'),
                Text('Podczas rejestracji w naszym serwisie wystąpił błąd.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      sleep(Duration(milliseconds: 300));
      firstCircle.currentState.fill();
    });
    final AuthService _auth = Provider.of<AuthService>(context, listen: false);
    DatabaseService db = Provider.of<DatabaseService>(context, listen: false);
    Map userData = db.userData;
    steps = [
      {
        'widget': () => 
        // AddAdress(
        //       formKey: _formKeyBasics,
        //       emailController: emailController,
        //     ),
        Attorney(),
        'nextFunc': () async {
          if (_formKeyBasics.currentState.validate()) {
            firstStripe.currentState.fill();
            sleep(Duration(milliseconds: 300));
            secondCircle.currentState.fill();

            var rng = new Random();
            var code = rng.nextInt(900000) + 100000;
            print(code);

            var result =
                await DatabaseService(uid: _auth.user.uid, listen: false)
                    .addNewAdress(
                        emailController.text, code, db.userData['raw']['name']);

            print(result);
            if (result == false) {
              _showMyDialog();
              return;
            }

            setState(() {
              secretCode = code.toString();
              currentStep = 1;
            });
          }
        },
      },
      {
        'widget': () => AddAdressCode(
              formkey: _formKeyAuthCode,
              secretCodeController: secretCodeController,
              secretCode: secretCode,
            ),
        'nextFunc': () async {
          if (_formKeyAuthCode.currentState.validate()) {
            secondStripe.currentState.fill();
            sleep(Duration(milliseconds: 300));
            thirdCircle.currentState.fill();
            DatabaseService(uid: _auth.user.uid, listen: false)
                .authorizeAdress(emailController.text);

            setState(() {
              currentStep = 2;
            });
          }
        },
      },
      {
        'widget': () => Attorney(),
        'nextFunc': () {
          http.get(
            'https://us-central1-moneyllama-2.cloudfunctions.net/sendAttorney' +
                '?name=${userData['raw']['name']}&surname=${userData['raw']['surname']}&uid=${_auth.user.uid}&email=${emailController.text}&phoneNumber=${userData['raw']['phoneNumber']}',
          );
          widget.goBack();
        },
      },
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CurvedShape(),
          SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.only(top: 38.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      widget.goBack();
                    },
                    iconSize: 40,
                    color: Colors.white,
                  ),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(6),
                          ),
                        ),
                        margin: EdgeInsets.symmetric(horizontal: 18),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 30),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FillCircle(key: firstCircle),
                                  FillStripe(key: firstStripe),
                                  FillCircle(key: secondCircle),
                                  FillStripe(key: secondStripe),
                                  FillCircle(key: thirdCircle),
                                ],
                              ),
                            ),
                            Expanded(
                              child: AnimatedSwitcher(
                                child: steps[currentStep]['widget'](),
                                duration: Duration(milliseconds: 500),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(18),
                    width: double.infinity,
                    child: RaisedButton(
                      onPressed: () {
                        steps[currentStep]['nextFunc']();
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      elevation: 0,
                      color: Color(0xffFED766),
                      child: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Text(
                          'Dalej',
                          style: TextStyle(
                              color: Color(0xffA0447E),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
