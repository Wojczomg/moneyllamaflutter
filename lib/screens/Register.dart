import 'dart:io';
import 'dart:math';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:moneylama2/screens/MainScreenWrapper.dart';
import 'package:moneylama2/services/auth_service.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:moneylama2/steps_widgets/create_account/attorney.dart';
import 'package:moneylama2/steps_widgets/create_account/auth-code.dart';
import 'package:moneylama2/steps_widgets/create_account/basics.dart';
import 'package:moneylama2/steps_widgets/create_account/passwords.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';
import 'package:moneylama2/widgets/FillCircle.dart';
import 'package:moneylama2/widgets/FillStripe.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  final Function goBack;
  RegisterScreen(this.goBack);
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  GlobalKey<FillCircleState> firstCircle = GlobalKey();
  GlobalKey<FillStripeState> firstStripe = GlobalKey();
  GlobalKey<FillCircleState> secondCircle = GlobalKey();
  GlobalKey<FillStripeState> secondStripe = GlobalKey();
  GlobalKey<FillCircleState> thirdCircle = GlobalKey();
  GlobalKey<FillStripeState> thirdStripe = GlobalKey();
  GlobalKey<FillCircleState> fourthCircle = GlobalKey();

  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController password1Controller = TextEditingController();
  TextEditingController password2Controller = TextEditingController();
  TextEditingController secretCodeController = TextEditingController();

  final _formKeyBasics = GlobalKey<FormState>();
  final _formKeyPasswords = GlobalKey<FormState>();
  final _formKeyAuthCode = GlobalKey<FormState>();

  var secretCode;

  List<Map> steps = [];

  int currentStep = 0;

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('AlertDialog Title'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Ups'),
                Text('Podczas rejestracji w naszym serwisie wystąpił błąd.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      sleep(Duration(milliseconds: 300));
      firstCircle.currentState.fill();
    });
    final AuthService _auth =
        Provider.of<AuthService>(context, listen: false).turnOffNotifier();

    steps = [
      {
        'widget': () => Basics(
              formKey: _formKeyBasics,
              nameController: nameController,
              surnameController: surnameController,
              emailController: emailController,
              phoneNumberController: phoneNumberController,
            ),
        'nextFunc': () {
          if (_formKeyBasics.currentState.validate()) {
            firstStripe.currentState.fill();
            sleep(Duration(milliseconds: 300));
            secondCircle.currentState.fill();

            print(nameController.text);
            print(surnameController.text);
            print(emailController.text);

            setState(() {
              currentStep = 1;
            });
          }
        },
      },
      {
        'widget': () => Passwords(
              formkey: _formKeyPasswords,
              password1Controller: password1Controller,
              password2Controller: password2Controller,
            ),
        'nextFunc': () async {
          if (_formKeyPasswords.currentState.validate()) {
            secondStripe.currentState.fill();
            sleep(Duration(milliseconds: 300));
            thirdCircle.currentState.fill();

            print(password1Controller.text);
            print(password2Controller.text);

            var rng = new Random();
            var code = rng.nextInt(900000) + 100000;
            print(code);
            var result = await _auth.registerWithEmailAndPassword(
              email: emailController.text,
              name: nameController.text,
              surname: surnameController.text,
              password: password2Controller.text,
              authCode: code.toString(),
              phoneNumber: phoneNumberController.text,
            );
            print(result);
            if (result == null) {
              _showMyDialog();
              return;
            }
            setState(() {
              secretCode = code.toString();
              currentStep = 2;
            });
          }
        },
      },
      {
        'widget': () => AuthCode(
              formkey: _formKeyAuthCode,
              secretCodeController: secretCodeController,
              secretCode: secretCode,
            ),
        'nextFunc': () {
          if (_formKeyAuthCode.currentState.validate()) {
            thirdStripe.currentState.fill();
            sleep(Duration(milliseconds: 300));
            fourthCircle.currentState.fill();
            DatabaseService(uid: _auth.user.uid, listen: false)
                .authorizeAdress(emailController.text);
            print(secretCodeController.text);
            setState(() {
              currentStep = 3;
            });
          }
        },
      },
      {
        'widget': () => Attorney(),
        'nextFunc': () {
          http.get(
            'https://us-central1-moneyllama-2.cloudfunctions.net/sendAttorney' +
                '?name=${nameController.text}&surname=${surnameController.text}&uid=${_auth.user.uid}&email=${emailController.text}&phoneNumber=${phoneNumberController.text}',
          );
          _auth.turnOnfNotifier().triggerNotify();
          // Navigator.of(context).pushReplacement(
          //     MaterialPageRoute(builder: (ctx) => MainScreenWrapper()));
        },
      },
    ];
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      print(constraints.maxHeight);
      print(constraints.maxWidth);
      return Stack(
        children: <Widget>[
          CurvedShape(),
          SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top: constraints.maxHeight/20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      widget.goBack();
                    },
                    iconSize: constraints.maxHeight/18.5,
                    color: Colors.white,
                  ),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(6),
                          ),
                        ),
                        margin: EdgeInsets.symmetric(horizontal: constraints.maxWidth/22),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.symmetric(vertical: constraints.maxHeight/24.5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FillCircle(key: firstCircle),
                                  FillStripe(key: firstStripe),
                                  FillCircle(key: secondCircle),
                                  FillStripe(key: secondStripe),
                                  FillCircle(key: thirdCircle),
                                  FillStripe(key: thirdStripe),
                                  FillCircle(key: fourthCircle),
                                ],
                              ),
                            ),
                            Expanded(
                              child: AnimatedSwitcher(
                                child: steps[currentStep]['widget'](),
                                duration: Duration(milliseconds: 500),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(constraints.maxWidth/22),
                    width: double.infinity,
                    child: RaisedButton(
                      onPressed: () {
                        steps[currentStep]['nextFunc']();
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      elevation: 0,
                      color: Color(0xffFED766),
                      child: Padding(
                        padding: EdgeInsets.all(constraints.maxHeight/52),
                        child: Text(
                          'Dalej',
                          style: TextStyle(
                              color: Color(0xffA0447E),
                              fontSize: constraints.maxHeight/37,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    });
  }
}
