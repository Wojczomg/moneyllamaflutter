import 'package:flutter/material.dart';
import 'package:moneylama2/services/auth_service.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:provider/provider.dart';

import 'AuthScreen.dart';
import 'MainScreenWrapper.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<AuthService>(context).user;

    // return either the Home or Authenticate widget
    if (user == null) {
      return AuthScreen();
    } else {
      return ChangeNotifierProvider<DatabaseService>(
        create: (_) =>DatabaseService(uid: user.uid),
        child: MainScreenWrapper(),
      );
    }
  }
}
