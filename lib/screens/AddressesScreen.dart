import 'package:flutter/material.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';

class AddressesScreen extends StatelessWidget {
  List emailDetailList;
  AddressesScreen(this.emailDetailList);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey[50],
      body: Stack(
        children: <Widget>[
          CurvedShape(),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    iconSize: 40,
                    color: Colors.white,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    child: Text(
                      'Twoje adresy email',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 22),
                    ),
                  ),
                  Expanded(
                    child: Card(
                      elevation: 8,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      // margin: EdgeInsets.all(18),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: ListView(
                          children: emailDetailList
                              .map(
                                (email) => Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          email['email'],
                                          style: TextStyle(
                                            fontSize: 19,
                                            color: Colors.grey[600],
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Column(
                                              children: <Widget>[
                                                Text(
                                                  'Zweryfikowany',
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: Colors.grey[600]),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                email['authorized']
                                                    ? Text(
                                                        'Tak',
                                                        style: TextStyle(
                                                          color: Colors.cyan,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      )
                                                    : Text(
                                                        'Nie',
                                                        style: TextStyle(
                                                          color: Colors.red,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Column(
                                              children: <Widget>[
                                                Text(
                                                  'Upoważniono',
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: Colors.grey[600]),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                email['attorney']
                                                    ? Text(
                                                        'Tak',
                                                        style: TextStyle(
                                                          color: Colors.cyan,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      )
                                                    : Text(
                                                        'Nie',
                                                        style: TextStyle(
                                                          color: Colors.red,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
