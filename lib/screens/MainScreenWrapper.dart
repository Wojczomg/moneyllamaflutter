import 'dart:math';

import 'package:flutter/material.dart';
import 'package:moneylama2/DUMMY_DATA.dart';
import 'package:moneylama2/screens/AddSubScreen.dart';
import 'package:moneylama2/screens/HomePage.dart';
import 'package:moneylama2/screens/SettingsScreen.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:moneylama2/steps_widgets/create_account/attorney.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';
import 'package:moneylama2/widgets/CustomBottomBar.dart';
import 'package:moneylama2/widgets/CustomFab.dart';
import 'package:moneylama2/widgets/SubsList.dart';
import 'package:moneylama2/widgets/TopPanel.dart';
import 'package:provider/provider.dart';

class MainScreenWrapper extends StatefulWidget {
  @override
  _MainScreenWrapperState createState() => _MainScreenWrapperState();
}

class _MainScreenWrapperState extends State<MainScreenWrapper> {
  Widget currentScreen = HomePage();
  Duration currentDuration = Duration(milliseconds: 200);
  AnimatedSwitcherTransitionBuilder currentTransition =
      AnimatedSwitcher.defaultTransitionBuilder;

  void goToHomePage() {
    setState(() {
      currentDuration = Duration(milliseconds: 200);
      currentTransition = AnimatedSwitcher.defaultTransitionBuilder;
      currentScreen = HomePage();
    });
  }

  void goToSettingsScreen() {
    setState(() {
      currentDuration = Duration(milliseconds: 200);
      currentTransition = AnimatedSwitcher.defaultTransitionBuilder;
      currentScreen = SettingsScreen();
    });
  }

  void goToAddSubScreen() {
    setState(() {
      currentDuration = Duration(milliseconds: 600);
      currentTransition = (Widget child, Animation<double> animation) {
        return ScaleTransition(child: child, scale: animation);
      };
      currentScreen = AddSubScreen(goToHomePage);
    });
  }

  Widget bottomBarBuilder(double height) {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      child: Container(
        height: height / 11,
        child: Padding(
          padding: const EdgeInsets.all(1),
          child: Row(children: <Widget>[
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        goToHomePage();
                      },
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Image.asset(
                              currentScreen is HomePage
                                  ? 'assets/icons/001-home-colored.png'
                                  : 'assets/icons/001-home.png',
                              width: double.infinity,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'Start',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {},
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Image.asset(
                              'assets/icons/002-edit.png',
                              width: double.infinity,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'Zarządzaj',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 30,
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {},
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Image.asset(
                              'assets/icons/001-console.png',
                              width: double.infinity,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'Ranking',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        goToSettingsScreen();
                      },
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Image.asset(
                              currentScreen is SettingsScreen
                                  ? 'assets/icons/004-settings-colored.png'
                                  : 'assets/icons/004-settings.png',
                              width: double.infinity,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'Ustawienia',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    DatabaseService db = Provider.of<DatabaseService>(context);
    Map userData = db.userData;

    if (userData == null) {
      return Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey[50],
        ),
      );
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey[50],
      body: Stack(
        children: <Widget>[
          CurvedShape(),
          AnimatedSwitcher(
            child: currentScreen,
            duration: currentDuration,
            switchInCurve: Curves.easeInOutQuart,
            switchOutCurve: Curves.easeInOutQuart,
            transitionBuilder: currentTransition,
          ),
        ],
      ),
      bottomNavigationBar: bottomBarBuilder(size.height),
      floatingActionButton: CustomFab(goToAddSubScreen),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
