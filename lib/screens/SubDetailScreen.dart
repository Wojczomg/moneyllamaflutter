import 'package:flutter/material.dart';
import 'package:moneylama2/screens/EditSubScreen.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';
import 'package:moneylama2/widgets/CustomBottomBar.dart';
import 'package:moneylama2/widgets/CustomFab.dart';
import 'package:provider/provider.dart';

import '../DUMMY_DATA.dart';

class SubDetailScreen extends StatelessWidget {
  final SubscriptionData sub;
  // final DatabaseService db;

  SubDetailScreen(this.sub);

  Future<void> _showMyDialog(context, DatabaseService db) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Uwaga'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(''),
                Text(
                    'Czy na pewno chcesz usunąć tą pozycję z rejestru swoich subskrypcji?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Tak'),
              onPressed: () async {
                await db.deleteService(
                  email: sub.email,
                  id: sub.id,
                );
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
            FlatButton(
              child: Text('anuluj'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showMyDialogCancelSub(context, DatabaseService db) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Uwaga'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(''),
                Text(
                    'Czy na pewno chcesz wysłać prośbę o anulowanie tej subskrypcji?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Tak'),
              onPressed: () async {
                await db.cancelSub(
                  sub: sub,
                );
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
            FlatButton(
              child: Text('Nie'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    DatabaseService db = Provider.of<DatabaseService>(context);
    return Scaffold(
      // bottomNavigationBar: CustomBottomBar(),
      // floatingActionButton:CustomFab((){}),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      backgroundColor: Colors.grey[50],
      body: Stack(
        children: <Widget>[
          CurvedShape(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                  },
                  iconSize: 40,
                  color: Colors.white,
                ),
                SizedBox(
                  height: 6,
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(6),
                          ),
                        ),
                        margin: EdgeInsets.symmetric(horizontal: 18),
                        child: Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  FlatButton.icon(
                                    label: Text(
                                      'Usuń',
                                      style: TextStyle(color: Colors.red),
                                    ),
                                    icon: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      _showMyDialog(context, db);
                                    },
                                  ),
                                  FlatButton.icon(
                                    label: Text('Edytuj'),
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.of(context).push(
                                        PageRouteBuilder(
                                          pageBuilder: (c, a1, a2) =>
                                              ChangeNotifierProvider<
                                                  DatabaseService>.value(
                                            value: db,
                                            child: EditSubScreen(
                                              () {
                                                Navigator.of(context).popUntil(
                                                    (route) => route.isFirst);
                                              },
                                              sub: sub,
                                            ),
                                          ),
                                          transitionsBuilder:
                                              (c, anim, a2, child) =>
                                                  FadeTransition(
                                                      opacity: anim,
                                                      child: child),
                                          transitionDuration:
                                              Duration(milliseconds: 400),
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                              Hero(
                                tag: '${sub.title}',
                                child: Image.asset(
                                  "${sub.imageUrl}",
                                  width: 160,
                                  height: 100,
                                  fit: BoxFit.contain,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                '${sub.title}',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              sub.isCancelled
                                  ? Text(
                                      'Anulowana',
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold),
                                    )
                                  : Text(
                                      '- ${sub.price} PLN',
                                      style: TextStyle(
                                          color: Colors.cyan,
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold),
                                    ),
                              SizedBox(
                                height: 1,
                              ),
                              sub.isCancelled
                                  ? Text('')
                                  : Text(
                                      'miesięcznie',
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey,
                                      ),
                                    ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                'Korzystasz z tej usługi już jakiś czas. Czy jesteś z niej zadowolony? Może to czas na znalezienie bardziej korzystnej oferty?',
                                style: TextStyle(
                                    fontSize: 20, color: Colors.grey[700]),
                              ),
                            ],
                          ),
                        )),
                  ),
                ),
                sub.isCancelled
                    ? Text('')
                    : Container(
                        margin: EdgeInsets.all(18),
                        width: double.infinity,
                        child: RaisedButton(
                          onPressed: () async {
                            _showMyDialogCancelSub(context, db);
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          elevation: 0,
                          color: Color(0xffFED766),
                          child: Padding(
                            padding: const EdgeInsets.all(14.0),
                            child: Text(
                              'Anuluj Subskrypcje',
                              style: TextStyle(
                                  color: Color(0xffA0447E),
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
