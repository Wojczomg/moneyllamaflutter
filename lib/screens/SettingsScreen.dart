import 'dart:math';

import 'package:flutter/material.dart';
import 'package:moneylama2/DUMMY_DATA.dart';
import 'package:moneylama2/screens/AddEmailScreen.dart';
import 'package:moneylama2/screens/AddressesScreen.dart';
import 'package:moneylama2/services/auth_service.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';
import 'package:moneylama2/widgets/CustomBottomBar.dart';
import 'package:moneylama2/widgets/CustomFab.dart';
import 'package:moneylama2/widgets/SubsList.dart';
import 'package:moneylama2/widgets/TopPanel.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DatabaseService db = Provider.of<DatabaseService>(context);
    Map userData = db.userData;

    return LayoutBuilder(
      builder: (contex, mainConstraints) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SafeArea(
            child: Padding(
              padding: EdgeInsets.only(
                top: (mainConstraints.maxWidth / 22),
                left: (mainConstraints.maxWidth / 22),
                right: (mainConstraints.maxWidth / 22),
              ),
              child: Text(
                'Zarządzaj aplikacją',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              ),
            ),
          ),
          Expanded(
            flex: 7,
            child: Card(
                elevation: 7,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7.0),
                ),
                margin: EdgeInsets.all(mainConstraints.maxWidth / 22),
                child: Padding(
                  padding: EdgeInsets.all(mainConstraints.maxWidth / 25),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Row(
                          children: <Widget>[
                            LayoutBuilder(
                              builder: (contex, constraints) => ClipRRect(
                                borderRadius: BorderRadius.circular(12),
                                child: Image.asset(
                                  'assets/llamahehe2.jpg',
                                  width: constraints.maxHeight,
                                  height: constraints.maxHeight,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Expanded(
                                      child: FittedBox(
                                        fit: BoxFit.scaleDown,
                                        alignment: Alignment.bottomLeft,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            Text(
                                              userData['raw']['name'] +
                                                  ' ' +
                                                  userData['raw']['surname'],
                                              style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Icon(
                                              Icons.check_circle,
                                              color: Colors.yellow,
                                              size: 30,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: FittedBox(
                                        fit: BoxFit.scaleDown,
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          userData["raw"]["mainEmail"],
                                          style: TextStyle(
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: (mainConstraints.maxWidth / 40),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          width: double.infinity,
                          height: double.infinity,
                          decoration: ShapeDecoration(
                            color: Colors.grey[200],
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  mainConstraints.maxWidth / 40),
                            ),
                          ),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                'assets/badges/badge1.png',
                                'assets/badges/badge2.png',
                                'assets/badges/badge3.png',
                                'assets/badges/badge4.png',
                                
                              ]
                                  .map((badge) => Padding(
                                        padding: EdgeInsets.all(
                                          mainConstraints.maxWidth / 18,
                                        ),
                                        child: Image.asset(badge),
                                      ))
                                  .toList(),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: mainConstraints.maxWidth / 22,
              vertical: mainConstraints.maxWidth / 100,
            ),
            child: Text(
              'Ogólne',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: mainConstraints.maxWidth / 18),
            ),
          ),
          Expanded(
            flex: 8,
            child: ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      PageRouteBuilder(
                        pageBuilder: (c, a1, a2) => AddressesScreen(
                          userData['emailDetailList'],
                        ),
                        transitionsBuilder: (c, anim, a2, child) =>
                            FadeTransition(opacity: anim, child: child),
                        transitionDuration: Duration(milliseconds: 400),
                      ),
                    );
                  },
                  child: Card(
                    margin: EdgeInsets.symmetric(horizontal: 18, vertical: 7),
                    color: Colors.white,
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Podgląd adresów',
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'Co tu moesz zrobić',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 16),
                              ),
                            ],
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.grey[300],
                            size: 32,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          ChangeNotifierProvider<DatabaseService>.value(
                        value: db,
                        child: AddEmailScreen(
                          () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ));
                  },
                  child: Card(
                    margin: EdgeInsets.symmetric(horizontal: 18, vertical: 7),
                    color: Colors.white,
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Nowy adres email',
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'Co tu moesz zrobić',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 16),
                              ),
                            ],
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.grey[300],
                            size: 32,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  margin: EdgeInsets.symmetric(horizontal: 18, vertical: 7),
                  color: Colors.white,
                  elevation: 0,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Powiadomienia',
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Co tu moesz zrobić',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.grey[300],
                          size: 32,
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    AuthService auth =
                        Provider.of<AuthService>(context, listen: false)
                            .turnOnfNotifier();
                    auth.signOut();
                  },
                  child: Card(
                    margin: EdgeInsets.symmetric(horizontal: 18, vertical: 7),
                    color: Colors.white,
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Wyloguj',
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 16),
                              ),
                            ],
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.grey[300],
                            size: 32,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
