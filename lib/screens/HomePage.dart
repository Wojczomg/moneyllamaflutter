import 'dart:math';

import 'package:flutter/material.dart';
import 'package:moneylama2/DUMMY_DATA.dart';
import 'package:moneylama2/widgets/CurvedShape.dart';
import 'package:moneylama2/widgets/CustomBottomBar.dart';
import 'package:moneylama2/widgets/CustomFab.dart';
import 'package:moneylama2/widgets/SubsList.dart';
import 'package:moneylama2/widgets/TopPanel.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController controller = ScrollController(
    // initialScrollOffset: 0.0,
    // keepScrollOffset: false,
  );
  bool closeTopContainer = false;

  @override
  void initState() {
    controller.addListener(() {
      setState(() {
        print(controller.offset);
        closeTopContainer = controller.offset > 50.0;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    print("building");
    return Container(
      height: size.height,
      width: size.width,
      child: SingleChildScrollView(
        controller: controller,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 18.0, left: 18, right: 18),
                child: Text(
                  'Podsumowanie',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 22),
                ),
              ),
            ),
            AnimatedOpacity(
              duration: const Duration(milliseconds: 300),
              opacity: closeTopContainer ? 0 : 1,
              child: AnimatedContainer(
                // height: size.height * 0.3,
                duration: const Duration(milliseconds: 300),
                height: closeTopContainer ? 0 : 220,
                child: TopPanel(),
              ),
            ),
            AnimatedOpacity(
              duration: const Duration(milliseconds: 200),
              opacity: closeTopContainer ? 0 : 1,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 18, vertical: 14),
                child: Text(
                  'Subskrypcje',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                ),
              ),
            ),
            SubsList(),
          ],
        ),
      ),
    );
    //     ],
    //   ),
    //   bottomNavigationBar: CustomBottomBar(),
    //   floatingActionButton: CustomFab(),
    //   floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    // );
  }
}
