import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

import 'db_service.dart';

class User {
  final String uid;
  User({this.uid});
}

class AuthService with ChangeNotifier {
  FirebaseAuth _auth;
  User user;
  Stream<FirebaseUser> stream;
  bool notifierOn = true;

  AuthService() {
    _auth = FirebaseAuth.instance;
    stream = _auth.onAuthStateChanged;
    stream.listen((onSnap) {
      user = _userFromFirebaseUser(onSnap);
      if (notifierOn) {
        notifyListeners();
      }
    });
  }

  // create user obj based on firebase user
  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }

  AuthService turnOffNotifier() {
    notifierOn = false;
    return this;
  }

  AuthService turnOnfNotifier() {
    notifierOn = true;
    return this;
  }

  void triggerNotify() {
    notifyListeners();
  }

  // sign in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  // register with email and password
  Future registerWithEmailAndPassword({
    String email,
    String password,
    String name,
    String surname,
    String authCode,
    String phoneNumber,
  }) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      await DatabaseService(uid: user.uid, listen: false).createUser(
        email,
        name,
        surname,
        authCode,
        phoneNumber,
      );
      return true;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  // sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error.toString());
      return null;
    }
  }
}
