import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:moneylama2/DUMMY_DATA.dart';
import 'package:uuid/uuid.dart';

var uuid = Uuid();

class DatabaseService with ChangeNotifier {
  String uid;
  DocumentReference userDocRef;
  Stream<DocumentSnapshot> stream;
  Map userData;
  bool notifierOn = true;

  DatabaseService({uid, listen = true}) {
    this.uid = uid;
    this.userDocRef = Firestore.instance.collection('users').document(uid);
    if (listen) {
      stream = userDocRef.snapshots();
      stream.listen((onSnap) {
        print('cos przyszlo');
        userData = modelUserData(onSnap.data);
        if (notifierOn) {
          notifyListeners();
        }
      });
    }
  }

  Future<bool> addNewAdress(String email, authCode, name) async {
    try {
      await Firestore.instance.collection('mail').add({
        'to': email,
        'message': {
          'subject': "Kod aktywacyjny",
          // 'html': '${authCode}',
          'html': """
        <p>Witaj $name</p>
        <p></p>
        <p>
        Do Twojego konta Money Llama został dodany ten ares mailowy.
W celu potwierdzenia adresu skopiuj poniższy kod i wklej w aplikacji mobilnej.  
        </p>
        <p></p>
        <p>$authCode</p>
        <p>
        W razie pytań znajdziesz nas na hello@moneyllama.com
        </p>
        """,
        }
      });
      await userDocRef.setData({
        'adresses': {
          '$email': {
            'authCode': authCode,
            'authorized': false,
            'attorney': false,
            'services': {},
          }
        }
      }, merge: true);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  void authorizeAdress(String email) {
    userDocRef.setData({
      'adresses': {
        '$email': {'authorized': true}
      }
    }, merge: true);
  }

  DatabaseService turnOffNotifier() {
    notifierOn = false;
    return this;
  }

  DatabaseService turnOnfNotifier() {
    notifierOn = true;
    return this;
  }

  void triggerNotify() {
    notifyListeners();
  }

  Map modelUserData(Map snapData) {
    int emailCount = snapData['adresses'].keys.length;
    int serviceCount = 0;
    double totalSum = 0.0;
    List servicesList = [];
    List emailList = [];
    List emailDetailsList = [];
    snapData['adresses'].forEach((k, v) {
      emailList.add(k);
      emailDetailsList.add({
        'email': k,
        'authorized': v['authorized'],
        'attorney': v['attorney'],
      });
      serviceCount += v['services'].length;
      for (var key in v['services'].keys) {
        var i = v['services'][key];
        totalSum += i['isCancelled'] ? 0 : i['price'];
        servicesList.add(
          SubscriptionData(
            id: key,
            email: k,
            price: i['price'],
            title: i['title'],
            imageUrl: i['imageUrl'],
            date: i['date'],
            isCancelled: i['isCancelled'],
            privacyDepEmail: i['privacyDepEmail'],
          ),
        );
      }
    });
    Map userdata = {
      'raw': snapData,
      'emailCount': emailCount,
      'serviceCount': serviceCount,
      'totalSum': totalSum,
      'serviceList': servicesList,
      'emailList': emailList,
      'emailDetailList': emailDetailsList,
    };
    return userdata;
  }

  Future<String> createUser(
    String email,
    String name,
    String surname,
    String authCode,
    String phoneNumber,
  ) async {
    // var rng = new Random();
    // var authCode = (rng.nextInt(900000) + 100000).toString();
    await userDocRef.setData({
      'mainEmail': email,
      'name': name,
      'surname': surname,
      'phoneNumber': phoneNumber,
      'adresses': {
        '$email': {
          'authCode': authCode,
          'authorized': false,
          'attorney': false,
          'services': {},
        },
      }
    });
    return authCode;
  }

  Future<bool> addNewService({
    String title,
    double price,
    String email,
    String date,
    String imageUrl,
    bool isCancelled = false,
    String privacyDepEmail,
  }) async {
    try {
      await userDocRef.setData({
        'adresses': {
          '$email': {
            'services': {
              '${uuid.v1()}': {
                'title': title,
                'price': price,
                'email': email,
                'date': date,
                'imageUrl': imageUrl,
                'isCancelled': isCancelled,
                'privacyDepEmail': privacyDepEmail,
              },
            },
          },
        }
      }, merge: true);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> deleteService({
    String email,
    String id,
  }) async {
    try {
      await userDocRef.setData({
        'adresses': {
          '$email': {
            'services': {'$id': FieldValue.delete()}
          },
        }
      }, merge: true);

      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  // Future<bool> editService({
  //   SubscriptionData sub,

  // }) async {
  //    try {
  //     await userDocRef.setData({
  //       'adresses': {
  //         '${sub.email}': {
  //           'services': {'${sub.id}': FieldValue.delete()}
  //         },
  //       }
  //     }, merge: true);

  //     return true;
  //   } catch (e) {
  //     print(e);
  //     return false;
  //   }
  // }
  Future<bool> cancelSub({
    SubscriptionData sub,
  }) async {
    try {
      await userDocRef.setData({
        'adresses': {
          '${sub.email}': {
            'services': {
              '${sub.id}': {
                'isCancelled': true,
              },
            },
          },
        },
      }, merge: true);
    } catch (e) {
      print(e);
      return false;
    }

    String envelopeId = userData['raw']['adresses'][sub.email]['envelopeId'];
    String documentId = userData['raw']['adresses'][sub.email]['documentId'];
    String mainEmail = userData['raw']['mainEmail'];
    // http.Response result =
    // await
    http.get(
        'https://us-central1-moneyllama-2.cloudfunctions.net/subCancelRequest?envelopeId=$envelopeId&documentId=$documentId&sendTo=$mainEmail');
    // if (result.statusCode == 500) {
    //   return false;
    // } else {
    return true;
    // }
  }
}
