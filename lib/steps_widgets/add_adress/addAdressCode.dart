import 'package:flutter/material.dart';

class AddAdressCode extends StatelessWidget {

  final formkey;
  final secretCodeController;
  final secretCode;

  AddAdressCode({
    this.formkey,
    this.secretCodeController,
    this.secretCode
  });

  @override
  Widget build(BuildContext context) {

    print(secretCode);

    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(25),
          child: Text(
            'Ok, teraz podaj kod, który otrzymałeś na skrzynkę mailową',
            style: TextStyle(
                color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 25),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Form(
            key: formkey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: secretCodeController,
                  validator: (value){
                    if(value != secretCode){
                      return "Kod nieprawidłowy";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Kod autoryzacji',
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}