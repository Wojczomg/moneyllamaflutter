import 'package:flutter/material.dart';

class AddAdress extends StatelessWidget {

  final formKey;
  final emailController;

  AddAdress({
    this.formKey,
    this.emailController,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(25),
          child: Text(
            'Podaj swój adres email',
            style: TextStyle(
                color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 25),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: emailController,
                  validator: (value){
                    if(value.isEmpty){
                      return 'Proszę wypełnić pole';
                    }
                    if(!value.contains('@')){
                      return 'Niepoprawny format adresu email';
                    }
                    if(value.length < 5){
                      return 'Niepoprawny format adresu email';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Wpisz adres',
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}


