import 'package:flutter/material.dart';

class Attorney extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(25),
          child: Text(
            'Już prawie gotowe! Jeszcze tylko potrzebujemy upowanienia od Ciebie, które pozwoli Ci zarządzać wszystkimi subskrypcjami z jednego miejsca. Po przejściu dalej wyślemy Ci je na maila. Podpisz je cyfrowo i postępuj zgodnie z instrukcjami.',
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.bold,
              fontSize: 24,
            ),
          ),
        ),
      ],
    );
  }
}
