import 'package:flutter/material.dart';

class Passwords extends StatelessWidget {

  final formkey;
  final password1Controller;
  final password2Controller;

  Passwords({
    this.formkey,
    this.password1Controller,
    this.password2Controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(25),
          child: Text(
            'Dzięki!\nTeraz podaj swoje hasło do konta Money Llama',
            style: TextStyle(
                color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 25),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Form(
            key: formkey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  obscureText: true,
                  controller: password1Controller,
                  validator: (value){
                    if(value.isEmpty){
                      return 'Proszę wypełnić to pole';
                    }
                    if(value.length < 6){
                      return 'Hasło musi zawierać przynajmniej 6 znaków';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Podaj hasło',
                  ),
                ),
                TextFormField(
                  obscureText: true,
                  controller: password2Controller,
                  validator: (value){
                    if(value.isEmpty){
                      return 'Proszę wypełnić to pole';
                    }
                    if(value.length < 6){
                      return 'Hasło musi zawierać przynajmniej 6 znaków';
                    }
                    if(value != password1Controller.text){
                      return 'Hasła muszą się zgadzać';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Powtórz hasło',
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}