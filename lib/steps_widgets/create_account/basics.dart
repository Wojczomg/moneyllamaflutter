import 'package:flutter/material.dart';

class Basics extends StatelessWidget {
  final formKey;
  final nameController;
  final surnameController;
  final emailController;
  final phoneNumberController;

  Basics({
    this.formKey,
    this.nameController,
    this.surnameController,
    this.emailController,
    this.phoneNumberController,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(25),
          child: Text(
            'Cześć, na początek podaj nam swoje podstawowe dane.',
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: nameController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Proszę wypełnić pole';
                    }
                    if (!(value[0].toUpperCase() == value[0])) {
                      return 'Imię musi zaczynać się z dużej litery';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Imię',
                  ),
                ),
                TextFormField(
                  controller: surnameController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Proszę wypełnić pole';
                    }
                    if (!(value[0].toUpperCase() == value[0])) {
                      return 'Nazwisko musi zaczynać się z dużej litery';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Nazwisko',
                  ),
                ),
                TextFormField(
                  controller: emailController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Proszę wypełnić pole';
                    }
                    if (!value.contains('@')) {
                      return 'Niepoprawny format adresu email';
                    }
                    if (value.length < 5) {
                      return 'Niepoprawny format adresu email';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Email',
                  ),
                ),
                TextFormField(
                  controller: phoneNumberController,
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Proszę wypełnić pole';
                    }
                    if (value.length < 8) {
                      return 'Niepoprawny format adresu email';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Numer telefonu',
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
