import 'package:flutter/material.dart';

const CURVE_HEIGHT = 280.0;

class CurvedShape extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height/2.63,
      child: CustomPaint(
        painter: _MyPainter(),
      ),
    );
  }
}

class _MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..style = PaintingStyle.fill
      ..isAntiAlias = true
      ..color = Color(0xffA0447E);

    Offset topLeft = Offset(0, 0);
    Offset bottomLeft = Offset(0, size.height * 0.7);
    Offset topRight = Offset(size.width, 0);
    Offset bottomRight = Offset(size.width, size.height * 0.3);
    Offset curvePoint = Offset(size.width * 0.6, size.height);

    Path path = Path()
      ..moveTo(topLeft.dx,
          topLeft.dy) // this move isn't required since the start point is (0,0)
      ..lineTo(bottomLeft.dx, bottomLeft.dy)
      ..quadraticBezierTo(
        curvePoint.dx,
        curvePoint.dy,
        bottomRight.dx,
        bottomRight.dy,
      )
      ..lineTo(topRight.dx, topRight.dy)
      ..close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
