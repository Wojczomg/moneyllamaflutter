import 'dart:math';

import 'package:flutter/material.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:provider/provider.dart';

Widget firstTabBuilder(totalSum, emailCount, serviceCount) {
  return LayoutBuilder(builder: (context, constraints) {
    
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: constraints.maxHeight / 17,
        horizontal: constraints.maxWidth / 12,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            totalSum != 0.0 ? '- $totalSum PLN' : '$totalSum PLN',
            style: TextStyle(
              color: Colors.cyan,
              fontSize: constraints.maxHeight / 5.6,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: constraints.maxHeight / 17,
          ),
          Text(
            'Miesięczne wydatki',
            style: TextStyle(
              fontSize: constraints.maxHeight / 9.4,
            ),
          ),
          SizedBox(
            height: constraints.maxHeight / 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    '$emailCount',
                    style: TextStyle(
                        color: Colors.cyan,
                        fontSize: constraints.maxHeight / 5.6,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Adresy',
                    style: TextStyle(
                      fontSize: constraints.maxHeight / 9.4,
                    ),
                  )
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                    '1',
                    style: TextStyle(
                        color: Colors.cyan,
                        fontSize: constraints.maxHeight / 5.6,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Konta',
                    style: TextStyle(
                      fontSize: constraints.maxHeight / 9.4,
                    ),
                  )
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                    '$serviceCount',
                    style: TextStyle(
                        color: Colors.cyan,
                        fontSize: constraints.maxHeight / 5.6,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Subskrypcji',
                    style: TextStyle(
                      fontSize: constraints.maxHeight / 9.4,
                    ),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  });
}

Widget secondTabBuilder(totalSum) {
  return LayoutBuilder(builder: (context, constraints) {
    return Stack(
      fit: StackFit.expand,
      children: [
        CustomPaint(
          painter: _MyPainter(totalSum),
        ),
        Container(
          padding: EdgeInsets.only(
            bottom: constraints.maxHeight/9.3,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                '$totalSum PLN',
                style: TextStyle(
                  color: Colors.cyan,
                  fontSize: constraints.maxHeight/4.8,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: constraints.maxHeight/16.8,
              ),
              Text(
                'z planowanych 700 PLN',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: constraints.maxHeight/9.3,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  });
}

class _MyPainter extends CustomPainter {
  double totalSum;
  _MyPainter(this.totalSum);
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true
      ..color = Colors.grey[300]
      ..strokeWidth = 14.0;

    Paint complete = new Paint()
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true
      ..color = Color(0xffFED766)
      ..strokeWidth = 14.0;

    Offset center = Offset(size.width / 2, size.height * 0.9);
    double radius = size.height * 0.8;

    canvas.drawArc(
      new Rect.fromCircle(center: center, radius: radius),
      -pi * 0.95,
      pi * 0.9,
      false,
      paint,
    );
    canvas.drawArc(
      new Rect.fromCircle(center: center, radius: radius),
      -pi * 0.95,
      (pi * 0.9) * totalSum / 700,
      false,
      complete,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class TopPanel extends StatefulWidget {
  @override
  _TopPanelState createState() => _TopPanelState();
}

class _TopPanelState extends State<TopPanel> {
  int currentPage = 0;
  PageController _controller;

  @override
  void initState() {
    _controller = PageController(initialPage: 0);
    _controller.addListener(() {
      int next = _controller.page.round();
      if (currentPage != next) {
        setState(() {
          currentPage = next;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DatabaseService db = Provider.of<DatabaseService>(context);
    Map userData = db.userData;

    return LayoutBuilder(builder: (context, constraints) {
      return Card(
        elevation: 7,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7.0),
        ),
        margin: EdgeInsets.all(18),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                height: double.infinity,
                width: double.infinity,
                child: PageView(
                    scrollDirection: Axis.horizontal,
                    controller: _controller,
                    children: [
                      firstTabBuilder(userData['totalSum'],
                          userData['emailCount'], userData['serviceCount']),
                      secondTabBuilder(userData['totalSum']),
                    ]),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  key: Key('0'),
                  margin: EdgeInsets.only(
                    bottom: constraints.maxHeight / 22,
                    left: constraints.maxWidth / 78,
                    right: constraints.maxWidth / 78,
                  ),
                  height: constraints.maxHeight / 37,
                  width: constraints.maxWidth / 13,
                  decoration: ShapeDecoration(
                    color:
                        currentPage == 0 ? Color(0xffA0447E) : Colors.grey[300],
                    shape: StadiumBorder(),
                  ),
                ),
                Container(
                  key: Key('1'),
                  margin: EdgeInsets.only(
                    bottom: constraints.maxHeight / 22,
                    left: constraints.maxWidth / 78,
                    right: constraints.maxWidth / 78,
                  ),
                  height: constraints.maxHeight / 37,
                  width: constraints.maxWidth / 13,
                  decoration: ShapeDecoration(
                    color:
                        currentPage == 1 ? Color(0xffA0447E) : Colors.grey[300],
                    shape: StadiumBorder(),
                  ),
                ),
              ],
            )
          ],
        ),
      );
    });
  }
}
