import 'package:flutter/material.dart';

class FillStripe extends StatefulWidget {
  FillStripe({Key key}) : super(key: key);
  @override
  FillStripeState createState() => FillStripeState();
}

class FillStripeState extends State<FillStripe> {
  double width = 0.0;

  void fill() {
    setState(() {
      width = 40;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal:6),
          height: 6,
          width: 40,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            color: Colors.grey,
          ),
        ),
        AnimatedContainer(
          margin: EdgeInsets.symmetric(horizontal:6),
          duration: Duration(milliseconds: 200),
          height: 6,
          width: width,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
            color: Color(0xffA0447E),
          ),
        ),
      ],
    );
  }
}
