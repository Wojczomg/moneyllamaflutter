import 'package:flutter/material.dart';
import 'package:moneylama2/screens/SubDetailScreen.dart';
import 'package:moneylama2/services/db_service.dart';
import 'package:provider/provider.dart';

import '../DUMMY_DATA.dart';

class SubsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DatabaseService db = Provider.of<DatabaseService>(context,);
    Map userData = db.userData;
    
    if (userData['serviceList'].isEmpty) {
      return Container(
        padding: EdgeInsets.all(20),
        child: Text(
          'Dodaj swoje subskrypcje',
          style: TextStyle(
            fontSize: 20,
            color: Colors.grey,
          ),
        ),
      );
    }
    return Column(
        // padding: EdgeInsets.all(0),
        children: userData['serviceList']
            .map<Widget>(
              (sub) => GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    PageRouteBuilder(
                      pageBuilder: (c, a1, a2) =>
                          ChangeNotifierProvider<DatabaseService>.value(
                        value: db,
                        child: SubDetailScreen(sub),
                      ),
                      transitionsBuilder: (c, anim, a2, child) =>
                          FadeTransition(opacity: anim, child: child),
                      transitionDuration: Duration(milliseconds: 400),
                    ),
                  );
                },
                child: Card(
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7.0),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      children: <Widget>[
                        Hero(
                          tag: '${sub.title}',
                          child: Image.asset(
                            sub.imageUrl,
                            width: 70,
                            height: 50,
                            fit: BoxFit.contain,
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                sub.title,
                                style: TextStyle(fontSize: 24),
                                overflow: TextOverflow.fade,
                                softWrap: false,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                sub.email,
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child:
                          sub.isCancelled ? Text(
                            'Anulowana',
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 25,
                                fontWeight: FontWeight.bold),
                          ) :
                           Text(
                            '- ${sub.price} PLN',
                            style: TextStyle(
                                color: Colors.cyan,
                                fontSize: 25,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
            .toList());
  }
}
