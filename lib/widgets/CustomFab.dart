import 'package:flutter/material.dart';
import 'package:moneylama2/screens/AddSubScreen.dart';

class CustomFab extends StatelessWidget {

  final Function goToAddSubScreen;

  CustomFab(this.goToAddSubScreen);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        backgroundColor: Color(0xffFED766),
        elevation: 0,
        onPressed: () {
          goToAddSubScreen();
        },
        child: Icon(
          Icons.add,
          size: 38,
          color: Color(0xffA0447E),
        ),
      );
  }
}