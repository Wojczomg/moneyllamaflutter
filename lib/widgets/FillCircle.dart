import 'package:flutter/material.dart';

class FillCircle extends StatefulWidget {
  FillCircle({Key key}) : super(key: key);
  @override
  FillCircleState createState() => FillCircleState();
}

class FillCircleState extends State<FillCircle> {

  double width = 0.0;

  void fill(){
    setState(() {
      width = 20;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: 20,
          width: 20,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.grey,
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 100),
          height: 20,
          width: width,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xffA0447E),
          ),
        ),
      ],
    );
  }
}
